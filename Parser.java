public class Parser {
    public static List<List<Object>> splitList(List<Object> items, Integer splitSize, Type destType) {
        // Take our destination type and cast to generic
        List<List<Object>> result = (List<List<Object>>)destType.newInstance();
        // Get a copy of source list to obtain list type.
        List<Object> protoList = items.clone();
        protoList.clear();
        // This is the list that will actually be added to result
        List<Object> tempList = protoList.clone();
        // A for loop with two counters.
        Integer index = 0, count = 0, size = items.size();
        while(index < size) {
            tempList.add(items[index++]);
            count++;
            // Split size reached, add to result and make new list
            if(count == splitSize) {
                result.add(tempList);
                tempList = protoList.clone();
                count = 0;
            }
        }
        // Add left-over partial
        if(!tempList.isEmpty()) {
            result.add(protoList);
        }
        return result;
    }
}